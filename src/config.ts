export const NATS_HOST = process.env.NATS_HOST || "localhost";
export const NATS_PORT = process.env.NATS_PORT ? Number(process.env.NATS_PORT) : 4222;

export const MS_IN_SECOND = 1000;
export const TIMEOUT_S = 20;
export const TIMEOUT_MS = TIMEOUT_S * MS_IN_SECOND;
