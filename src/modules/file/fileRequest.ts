import { createWriteStream, WriteStream } from "fs";
import NATS from "nats";
import { TIMEOUT_MS, TIMEOUT_S } from "../../config";
import { DataType } from "../../index";
import log from "../../utils/log";

interface IRequestOptions {
  nats: NATS.Client;
  source: string;
  destination?: string;
}

enum EventType {
  Error = "error",
  End = "end",
  Data = "data",
}

class FileRequest {
  private nats: NATS.Client;
  private source: string;
  private destination?: string;
  private _running: boolean;
  private actions: {[key: string]: (...args: any) => void};
  private writeStream?: WriteStream;
  private sid: number;

  constructor(options: IRequestOptions) {
    const { nats, source, destination } = options;

    this.nats = nats;
    this.source = source;
    this.destination = destination;
    this._running = false;
    this.actions = {};
    this.sid = 0;
  }

  get running() {
    return this._running;
  }

  public on(event: EventType, action: (err?: Error, data?: any) => any) {
    this.actions[event] = action;

    if (event === EventType.Data && !this.running) {
      this.run();
    }

    return this;
  }

  public run() {
    const { nats, source, destination } = this;

    log.info(`Send request of file ${source}...`);

    if (destination) {
      log.info(`File will be saved to ${destination}`);
    }

    this.sid = nats.request(DataType.File, source, (response: Buffer, replyTo: string) => {
      log.debug("NATS response received");
      this.handleResponse(response, replyTo);
    });
    log.debug("File request sid:", this.sid);

    // XXX: The following replaces request own timeout
    // TODO: Replace request with own inbox subscription
    /*const minMessages = 10;
    nats.timeout(this.sid, TIMEOUT_MS, minMessages, () => {
      this.error(new Error(`Request timed out after ${TIMEOUT_S} seconds`));
    });*/

    this.setRunning(true);
  }

  public end(success = true) {
    log.info("Request end");
    const writeStream = this.writeStream;
    const endAction = this.actions[EventType.End];

    writeStream && writeStream.end();
    success && endAction && endAction();

    // this.actions = {};
    this.nats.unsubscribe(this.sid);
    this.setRunning(false);
  }

  private error(err: Error) {
    log.error("File request error:", err);
    const action = this.actions[EventType.Error];
    action && action(err);
    this.end(false);
  }

  private handleResponse(response: Buffer, replyTo: string) {
    const { destination } = this;

    if (!this.writeStream && destination) {
      log.debug("Creating write stream...");
      this.writeStream = createWriteStream(destination);
    }

    const dataSize = response.length;

    log.debug(`Response ${dataSize} bytes received`);
    log.debug(`Reply to: ${replyTo}`);

    if (replyTo) {
      log.debug("Confirm receiving response with 'ok'...");
      this.nats.publish(replyTo, "ok");
    }

    const writeStream = this.writeStream;

    const isData = dataSize > 0;

    if (isData) {
      const dataAction = this.actions[EventType.Data];

      writeStream && writeStream.write(response);
      dataAction && dataAction(null, response);
    } else {
      this.nats.flush(() => this.end());
    }
  }

  // XXX: Private setters with public getters are not supported
  private setRunning(value: boolean) {
    this._running = value;
  }
}

export default FileRequest;
export { EventType };
