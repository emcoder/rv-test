import NATS from "nats";
import { DataType } from "../../../transport";
import FileResponse from "../fileResponse";

jest.mock("nats", () => ({
  connect: jest.fn(() => ({
    request: jest.fn((_type: DataType, source: string, action) => {
      const response = source === "close"
        ? Buffer.alloc(0)
        : Buffer.from("hello");

      action(response, "replyTo");
      return 10;
    }),
    publish: jest.fn(),
    flush: jest.fn((action) => action()),
    unsubscribe: jest.fn(),
    timeout: jest.fn(),
  })),
  createInbox: jest.fn(),
}));

jest.mock("fs");

describe("File response tests", () => {
  let nats: NATS.Client;
  let fileResponse: FileResponse;

  beforeAll(() => {
    const options = {};
    nats = NATS.connect(options);
  });

  beforeEach(() => {
    fileResponse = new FileResponse({
      nats,
      path: "test.txt",
      to: "replyTo",
    });
  });

  /*it("Should call createReadStream on run", () => {

  });*/
});
