import NATS from "nats";
import { DataType } from "../../../transport";
import FileRequest, { EventType } from "../fileRequest";

jest.mock("nats", () => ({
  connect: jest.fn(() => ({
    request: jest.fn((_type: DataType, source: string, action) => {
      const response = source === "close"
        ? Buffer.alloc(0)
        : Buffer.from("hello");

      action(response, "replyTo");
      return 10;
    }),
    publish: jest.fn(),
    flush: jest.fn((action) => action()),
    unsubscribe: jest.fn(),
    timeout: jest.fn(),
  })),
}));

jest.mock("fs");

describe("File request tests", () => {
  let nats: NATS.Client;
  let fileRequest: FileRequest;

  beforeAll(() => {
    const options = {};
    nats = NATS.connect(options);
  });

  beforeEach(() => {
    fileRequest = new FileRequest({
      nats,
      source: "test.txt",
      destination: "copy.txt",
    });
  });

  it("Should response with ok on received data", () => {
    fileRequest.run();

    expect(nats.publish).toBeCalledWith("replyTo", "ok");
  });

  it("Should run request on data event handler attach", () => {
    expect(fileRequest.running).toBeFalsy();

    fileRequest.on(EventType.Data, jest.fn());

    expect(fileRequest.running).toBeTruthy();
  });

  it("Should correctly end request on empty buffer", () => {
    fileRequest = new FileRequest({
      nats,
      source: "close",
    });

    const onEnd = jest.fn();

    fileRequest.on(EventType.End, onEnd);
    fileRequest.run();

    expect(nats.publish).toBeCalledWith("replyTo", "ok");
    expect(nats.flush).toBeCalled();
    expect(onEnd).toBeCalled();

    // FIXME: Unsubscribe receiving incorrect sid from constructor
    // expect(nats.unsubscribe).toBeCalledWith(10);
    expect(nats.unsubscribe).toBeCalled();
  });

});
