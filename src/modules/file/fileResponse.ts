import { EventEmitter } from "events";
import { createReadStream, ReadStream } from "fs";
import NATS, { createInbox, NatsError } from "nats";
import { TIMEOUT_MS, TIMEOUT_S } from "../../config";
import log from "../../utils/log";
import { EventType } from "./fileRequest";

const CHUNK_SIZE = 1024 * 1024; // 1 MiB in bytes

// Number of chunks stored before send them to client
// FIXME: Currently order of chunks delivery is not guaranteed on > 1 number, so data corruption may occur
const CHUNK_QUEUE_SIZE = 1;

interface IResponseOptions {
  nats: NATS.Client;
  to: string;
  path: string;
}

class FileResponse {
  private nats: NATS.Client;
  private to: string;
  private path: string;
  private actions: {[key: string]: (...args: any) => void};
  private buffer: Buffer[];
  private inbox: string;
  private sids: number[];
  private events: EventEmitter;
  private readStream?: ReadStream;

  constructor(options: IResponseOptions) {
    const { nats, to, path } = options;

    this.nats = nats;
    this.to = to;
    this.path = path;
    this.actions = {};
    this.buffer = [];
    this.inbox = createInbox();
    this.sids = [];
    this.events = new EventEmitter();

    this.events.on(EventType.Error, () => null);
  }

  public on(event: EventType | string, action: (...args: any) => void): FileResponse {
    log.debug("Added action on response event", event.toString());
    this.actions[event] = action;
    return this;
  }

  public end(success = true) {
    const { nats, readStream, sids, actions } = this;
    readStream && readStream.close();

    sids.forEach((sid) => {
      nats.unsubscribe(sid);
    });

    const endAction = actions[EventType.End];
    success && endAction && endAction();
  }

  public run() {
    const { path } = this;

    const readStream = createReadStream(path, { highWaterMark: CHUNK_SIZE });
    readStream
      .on("readable", () => {
        this.handleReadable(readStream);
      })
      .on("error", (err) => {
        const error = new Error(`File read error: ${err}`);
        this.error(error);
      })
      .on("end", async () => {
        await this.handleReadEnd();
      });
    this.readStream = readStream;

    this.subscribe();
  }

  private handleReadable(readStream: ReadStream) {
    const { buffer } = this;

    if (buffer.length < CHUNK_QUEUE_SIZE) {
      this.readChunk(readStream);
    } else {
      log.debug("Buffer is full, wait for flush...");
      this.flush()
        .then(() => {
          this.readChunk(readStream);
        })
        .catch((err) => {
          this.error(err);
        });
    }
  }

  private async handleReadEnd() {
    const { buffer } = this;

    log.info("File read end");
    if (buffer.length > 0) {
      try {
        await this.flush();
      } catch (err) {
        this.error(err);
      }
    }
    log.debug("Publish end message...");

    try {
      await this.publish(Buffer.alloc(0));
      log.debug("End confirmation received");
    } catch (err) {
      log.error("End confirmation not received!");
    } finally {
      this.end();
    }
  }

  private subscribe() {
    const { nats, inbox, events } = this;

    // Handle control messages from request client
    log.debug(`Subscribing on response inbox ${inbox}...`);
    const sid = nats.subscribe(inbox, (message: Buffer | string) => {
      const str = message.toString("utf8");
      log.debug("Incoming message received:", str);
      if (str === "ok") {
        events.emit("ok");
      } else {
        log.warn("Unknown message:", str);
      }
    });

    this.sids.push(sid);
  }

  private error(err: Error) {
    log.error("File response error:", err);
    const action = this.actions[EventType.Error];
    action && action(err);
    this.end(false);
  }

  private readChunk(stream: ReadStream) {
    log.debug("Read next file chunk...");
    const chunk = stream.read();
    if (!chunk) {
      log.debug("Received null chunk");
      return;
    }

    log.debug(`Add to buffer chunk of size ${chunk.length} bytes`);
    this.buffer.push(chunk);
    log.debug("Buffer size:", this.buffer.length);
  }

  // Send chunk of data and wait for receiving confirmation
  private publish(data: Buffer): Promise<void> {
    return new Promise((resolve, reject) => {
      const { nats, to, inbox } = this;
      const dataSize = data.length;

      log.debug(`Publishing ${dataSize} bytes of data...`);

      nats.publish(to, data, inbox, (error?: NatsError) => {
        if (error) {
          log.error(`Failed to publish ${dataSize} bytes: ${error.toString()}`);
          reject(error);
        }

        log.debug(`Published ${dataSize} bytes`);

        log.debug("Waiting for receive confirmation...");

        this.wait("ok").then(() => {
          log.debug("Confirmation received");
          resolve();
        }).catch((err) => {
          log.error("Confirmation failed:", err);
          reject(err);
        });
      });
    });
  }

  private wait(message: string) {
    let timeoutId: any;
    const timeoutPromise = new Promise((resolve, reject) => {
      timeoutId = setTimeout(() => {
        clearTimeout(timeoutId);
        reject(new Error(`Timed out in ${TIMEOUT_S} seconds`));
      }, TIMEOUT_MS);
    });

    const okPromise = new Promise((resolve, reject) => {
      this.events.once(message, () => {
        clearTimeout(timeoutId);
        resolve();
      });
    });

    return Promise.race([okPromise, timeoutPromise]);
  }

  private async flush() {
    const chunkCount = this.buffer.length;

    log.info(`Flush ${chunkCount} buffered chunks...`);

    // FIXME: Can this cause corrupted data due to out-of-order chunks received?
    const promises = this.buffer.map(async (chunk) => {
      await this.publish(chunk);
    });

    try {
      await Promise.all(promises);
    } catch (err) {
      const error = new Error(`Buffer flush error: ${err}`);
      this.error(error);
    } finally {
      this.buffer = [];
    }

    /*while (this.buffer.length > 0) {
      const chunk = this.buffer.shift();
      if (!chunk) {
        return;
      }
      await this.publish(chunk);
      log.info("Published chunk");
    }*/
  }
}

export default FileResponse;
