import fs, { PathLike } from "fs";

function isFromEnum(value: string, enumType: any): boolean {
  return Object.values(enumType).includes(value);
}

function capitalize(value: string): string {
  return value.charAt(0).toUpperCase() + value.slice(1);
}

function toEnum<E>(value: string, enumType: E): E[keyof E] {
  const valueStr = capitalize(value.toString());
  const typeStr = valueStr as keyof typeof enumType;
  const enumValue = enumType[typeStr];
  return enumValue;
}

// fs.exists promise wrapper
function fsExists(path: PathLike) {
  return new Promise((resolve) => {
    fs.exists(path, (exists) => {
      resolve(exists);
    });
  });
}

export { isFromEnum, fsExists, capitalize, toEnum };
