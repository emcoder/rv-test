enum Level {
  Debug = "DEBUG",
  Info = "INFO",
  Warn = "WARN",
  Error = "ERROR",
}

let enabled = false;

function prefix(level = Level.Info) {
  return `[${timestamp()}] (${level.toString()}) ⇒`;
}

function timestamp() {
  const date = new Date();
  return date.toLocaleString();
}

function info(...args: any) {
  enabled && console.info(prefix(), ...args);
}

function error(...args: any) {
  enabled && console.error(prefix(Level.Error), ...args);
}

function warn(...args: any) {
  enabled && console.warn(prefix(Level.Warn), ...args);
}

const log = info; // Alias for `info`

function debug(...args: any) {
  enabled && console.debug(prefix(Level.Debug), ...args);
}

function enable(value: boolean) {
  enabled = value;
}

export default {
  log,
  info,
  debug,
  warn,
  error,
  enable,
};
