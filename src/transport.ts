import NATS from "nats";
import { NATS_HOST, NATS_PORT } from "./config";
import { FileRequest, FileResponse } from "./modules/file";
import { isFromEnum, toEnum } from "./utils";
import log from "./utils/log";

interface IRequestOptions {
  type: DataType;
  source?: string;
  destination?: string;
}

interface IResponseOptions {
  type: DataType;
  to: string;
  path?: string;
}

interface ITransportOptions {
  nats?: string;
  log?: boolean;
}

enum DataType {
  File = "file",
}

enum EventType {
  Connect = "connect",
  Error = "error",
}

type Action = (...args: any) => void;

class Transport {
  private nats: NATS.Client;
  private sids: number[];
  private actions: {[type: string]: Action};

  constructor(options?: ITransportOptions) {
    const {
      nats: url = `nats://${NATS_HOST}:${NATS_PORT}`,
      log: isLog = false,
    } = options || {};

    log.enable(isLog);

    this.nats = NATS.connect({
      url,
      preserveBuffers: true,
    });
    this.sids = [];
    this.actions = {};

    this.nats.on(EventType.Connect, () => {
      log.debug(`NATS transport on ${url} connected`);

      const action = this.actions[EventType.Connect];
      action && action();
    });

    this.nats.on(EventType.Error, (err) => {
      log.error(`NATS error occured: ${err}`);

      const action = this.actions[EventType.Error];
      action && action();
    });
  }

  public on(type: DataType | EventType, action: Action) {
    if (isFromEnum(type, DataType)) {
      const dataType = toEnum(type, DataType);
      this.onData(dataType, action);
    } else if (isFromEnum(type, EventType)) {
      const eventType = toEnum(type, EventType);
      this.onEvent(eventType, action);
    } else {
      throw new Error(`Unknown type: ${type.toString()}` +
        ", neither DataType, nor EventType");
    }
  }

  public response(options: IResponseOptions) {
    const { type, to } = options;

    log.info(`Creating response of type "${type.toString()}"`);
    log.debug(`Response receiver: ${to}`);

    if (type === DataType.File) {
      const { path } = options;

      if (!path) {
        throw new Error("For file response type 'path' parameter is required");
      }

      const res = new FileResponse({
        nats: this.nats,
        to,
        path,
      });
      res.run();

      return res;
    }

    throw new Error(`Unknown response data type: ${type}`);
  }

  public request(options: IRequestOptions) {
    const { type } = options;

    log.info(`Creating request of type "${type.toString()}"`);

    if (type === DataType.File) {
      const { source, destination } = options;

      if (!source) {
        throw new Error("File request requires 'source' option");
      }

      const req = new FileRequest({
        nats: this.nats,
        source,
        destination,
      });

      // Start transfer, as we know where to save it
      // If no destination was set — request will be runned when 'data' event handler connected
      if (destination) {
        log.debug("Destination path provided, run request...");
        req.run();
      }

      return req;
    }

    log.error(`Unknown request data type: ${type}`);
    throw new Error(`Unknown request data type: ${type}`);
  }

  public close() {
    this.nats.flush(() => {
      this.sids.forEach((sid) => {
        this.nats.unsubscribe(sid);
      });
      this.nats.close();
      log.info("Transport closed");
    });
  }

  private onData(type: DataType, action: Action) {
    log.info(`Transport will handle "${type.toString()}" event`);

    const sid = this.nats.subscribe(type, (request: string, replyTo: string) => {
      log.info(`Received "${type.toString()}" request: "${request}"`);
      action(replyTo, request);
    });
    this.sids.push(sid);

    return this;
  }

  private onEvent(type: EventType, action: Action) {
    log.info(`Transport subscribe on ${type.toString()} event`);
    this.nats.on(type, (...args) => action(...args));

    return this;
  }

}

export default Transport;
export { DataType };
export { EventType };
