// Example file copy utility
const { Transport, DataType, EventType } = require("../dist");

const NATS_HOST = envOr("NATS_HOST", "localhost");
const NATS_PORT = envOr("NATS_PORT", 4222);

function main() {
  const { source, destination, verbose } = args();

  verbose && console.info("Running in verbose mode");
  
  console.log(`Copy file ${source} to ${destination}`);

  const transport = new Transport({
    nats: `nats://${NATS_HOST}:${NATS_PORT}`,
    log: verbose
  });

  transport.on(EventType.Connect, () => {
    console.log('Transport connected!');
  });

  transport.on(EventType.Error, (err) => {
    console.error("Transport error:", err);
    transport.close();
  });

  // FILE SERVER
  serve(transport);

  // FILE REQUEST
  request(transport, source, destination);

}

function request(transport, source, destination) {
  console.log(`Sending request of file ${source}...`);

  const req = transport.request({
    type: DataType.File,
    source,
    destination,
  });

  req.on("error", (err) => {
    console.error(`File ${source} request failed: ${err}`);
    transport.close();
  });

  req.on("end", () => {
    console.error(`File ${source} saved to ${destination}`);
    transport.close();
  });
}

function serve(transport) {
  console.log('Start listen to file requests...');

  transport.on(DataType.File, (replyTo, path) => {
    console.log(`File ${path} request accepted`);
    console.log(`Send file ${path}...`);

    const res = transport.response({
      type: DataType.File,
      to: replyTo,
      path,
    });

    res.on("error", (err) => {
      console.error("Response error:", err);
    });

    res.on("end", () => {
      console.log(`Requested file ${path} was sent`);
    });
  });
}

function envOr(varName, defaultValue) {
  let value = process.env[varName];
  if (value && typeof defaultValue === "number") {
    value = Number(value);
  }
  return value || defaultValue;
}

function args() {
  let args = process.argv.slice(2); // Skip `node` and name of JS-file

  let verbose = false;
  const usage = "Usage: node ./copy.js [-v] SOURCE DEST"

  args = args.reduce((fileArgs, arg) => {
    switch (arg) {
      case "-h":
        console.info(usage);
        process.exit();
      case "-v":
        verbose = true;
        break;
      default:
        fileArgs.push(arg);
    }

    return fileArgs;
  }, []);

  const [ source, destination ] = args;

  if (!source) {
    console.error("Missing source parameter!\n" + usage);
    process.exit(1);
  }
  if (!destination) {
    console.error("Missing destination parameter!\n" + usage);
    process.exit(1);
  }

  return { source, destination, verbose };
}

main();