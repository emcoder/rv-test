# RV Test
Big files transport created with **Node.js** and **TypeScript**, based on [NATS](https://www.npmjs.com/package/nats) and implemented as *test assignment*.

## Requirements
Transport requires already running [NATS server](https://github.com/nats-io/gnatsd).  
### NATS server
NATS server host and port can be set for new `Transport` object in optional `nats` parameter:
```javascript
const transport = new Transport({
  nats: "nats://localhost:4222",
  log: true
});
```
Also transport will use environment variables `NATS_HOST` and `NATS_PORT`.  
Default values — host: `localhost` and port: `4222`.

## Example
Example file copy utility implemented with transport can be found in [`example/copy.js`](example/copy.js) file.  
Usage: `node ./copy.js [-v] SOURCE DEST`  

Example usage in verbose mode (with transport debug output) and custom NATS port:
```bash
git clone https://gitlab.com/fludardes/rv-test.git
cd rv-test/example
env NATS_PORT=4242 node ./copy.js -v /path/to/source.data /path/to/destination.data
```